"""Modules for third-party or work-in-progress contributions.

    The contrib module contains community contributions and
    experimental classes/functions for omni. It is expected
    that any code under this module has not undergone rigorous
    testing and may be unstable.
"""
