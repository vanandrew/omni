# pylint: disable=unused-import,redefined-outer-name
import unittest
import os
import nibabel as nib
from omni import preprocessing
from . import TESTDATA
from . import plumb_oblique_data


class TestPreprocessing(unittest.TestCase):
    def test_saturate(self):
        # load test image
        img = nib.load(os.path.join(TESTDATA, "imgs", "test_plumb.nii.gz"))

        # test image
        img2 = preprocessing.saturate(img)
        self.assertIsInstance(img2, nib.Nifti1Image)

    def test_equalize(self):
        # load test image
        img = nib.load(os.path.join(TESTDATA, "imgs", "test_plumb.nii.gz"))

        # test image
        img2 = preprocessing.equalize(img)
        self.assertIsInstance(img2, nib.Nifti1Image)

    def test_localized_contrast_enhance(self):
        # load test image
        img = nib.load(os.path.join(TESTDATA, "imgs", "test_plumb.nii.gz"))
        # load test mask
        mask = nib.load(os.path.join(TESTDATA, "imgs", "test_mask.nii.gz"))

        # test image
        img2 = preprocessing.localized_contrast_enhance(img, mask)
        self.assertIsInstance(img2, nib.Nifti1Image)


def test_clahe(plumb_oblique_data):
    test_data = plumb_oblique_data[0]
    img = nib.load(test_data)
    output_img = preprocessing.clahe(img)
    assert output_img.shape == img.shape


def test_normalization(plumb_oblique_data):
    test_data = plumb_oblique_data[0]
    img = nib.load(test_data)
    output_img = preprocessing.normalization(img)
    data = output_img.get_fdata()
    assert data.ravel().max() == 1
    assert data.ravel().min() == 0
