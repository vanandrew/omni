# pylint: disable=unused-import,redefined-outer-name
import numpy as np
from omni import masks
from . import fake_epi_and_mask

# create fixed random state
rng = np.random.RandomState(777)


def test_lda():
    test_data = np.concatenate([rng.rand(50, 10) + 10, rng.rand(50, 10)], axis=0)
    test_labels = np.concatenate([np.ones(50), np.zeros(50)], axis=0).astype(bool)
    result = masks.lda(test_data, test_labels)
    assert test_data.shape == result.shape


def test_generate_noise_mask(fake_epi_and_mask):
    fake_data_img = fake_epi_and_mask[0]
    mask_img = fake_epi_and_mask[1]
    noise_mask, smooth_noise_mask = masks.generate_noise_mask(fake_data_img, mask_img, iterations=2)
    assert mask_img.shape == noise_mask.shape
    assert mask_img.shape == smooth_noise_mask.shape
