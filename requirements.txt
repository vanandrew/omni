# Auto-generated on 2022-10-25T11:52:43.269727
nibabel >= 3.2.2
numpy >= 1.21.5
pybids >= 0.12.2
scikit-image >= 0.19.0
h5py >= 2.10.0
toml >= 0.10.2
memori >= 0.1.5

myst-parser >= 0.13.5
numpydoc >= 1.1.0
sphinx >= 4, < 6
pydata-sphinx-theme >= 0.8.1

black >= 20.8b1
coverage[toml] >= 5.5
pytest == 7.1.3
pytest-pycodestyle >= 2.2.0

nipy >= 0.5.0
nipype >= 1.7.1
sdcflows >= 2.0.13
