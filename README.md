# omni

`omni` is a Python/C++17 library for image preprocessing in neuroimaging and
other related things. It provides some useful and convenient APIs for developing 
neuroimaging tools. These are used to develop implementations of software tools
I (and others) are currently developing.

Current software tools list:

- ### [Synth](#synth-1)

### Note

If any of the `omni` utilities significantly assist with your research, please cite:
```
Montez, D. F., Van, A. N., Miller, R. L., Seider, N. A., Marek, S., Zheng, A., et al. (2021).
Using synthetic MR images for field map-less distortion correction.
bioRxiv, 2021.03.13.435270. http://doi.org/10.1101/2021.03.13.435270
```

## Documentation

See [Documentation](https://omnineuro.readthedocs.io/en/latest/?badge=latest).

## Requirements and Installation

See [Installation](https://omnineuro.readthedocs.io/en/latest/installation.html).

### Synth

See [Synth Documentation](https://omnineuro.readthedocs.io/en/latest/synth/index.html) for more information.
