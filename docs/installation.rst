.. _installation:

Installation
############

Overview
********

``omni`` is a hybrid Python/C++17 library designed for developing image
preprocessing algorithms.

System Requirements
*******************

A linux system is required to run ``omni``. Currently, the recommended linux
OS to use is ``Ubuntu 20.04``.

It is recommended that your system has at least 32 GB of RAM to run ``omni``.
Actual memory usage will depend on the size of the images, and the models
that you are using.

``omni`` currently relies on programs from `FSL`_, `AFNI`_, and `ANTs`_ for some functionality.
The list below are the known versions for each package known to work with ``omni``:

.. code-block:: bash

    # Newer/Older versions may still work, but have not been tested.
    FSL 6.0.4
    AFNI 20.0.18
    ANTs 15a6861

To install these packages, refer to each package's respective documenation.

Installing ``omni``
*******************

``omni`` has a PyPI package available `here <https://pypi.org/project/omnineuro/>`_. To install it, you can
use ``pip``:

.. warning::
    The PyPI package for ``omni`` is called ``omnineuro``. The ``omni`` package on PyPI
    refers to an unrelated package. Take care to install the correct package!

.. code-block:: bash

    # base installation of omni package
    pip install omnineuro

    # OR

    # if you want to use SpaceTimeRealign (Simultaneous Slice Time/Motion Correction)
    # you need to install the extra packages
    pip install omnineuro[extra]

    # post installation
    omni_affine -h
    # should run successfully and give you the help text for the omni_affine script
    # note that on first run, it may take some time due to python generating bytcode files

.. _omni-source-build:

Building and installing ``omni`` from source
********************************************

While the ``omni`` package is available on PyPI, you may have reason to build and install ``omni`` directory from
source. In this case, you will need to install a few system dependencies to compile the C++ modules.

System Dependencies
===================

You will need ``python >=3.6``, ``pip >= 19.1``, ``gcc >=8``,
``gfortran >=8``, ``openblas``, ``hdf5``, and ``fftw3``
Example package manager commands are below:

``Ubuntu`` and ``Debian``
-------------------------

.. code-block:: bash

    # this was tested on Ubuntu 20.04 and 18.04
    apt-get install -y python3 python3-dev python3-distutils python3-pip build-essential \
    gfortran libopenblas-dev liblapack-dev libhdf5-dev libfftw3-dev

``Centos``
----------

.. code-block:: bash
    
    # this was tested on Centos 7

    # add the epel repo
    yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

    # dependencies
    yum -y install python3-devel openblas-devel lapack-devel hdf5-devel fftw3-devel

    # pip version on Centos is old, so we use pip to update itself
    pip3 install pip -U

    # install devtoolset for gcc/gfortran 8
    yum -y install centos-release-scl && yum -y install devtoolset-8

    # this enables the gcc/gfortran toolset
    source /opt/rh/devtoolset-8/enable

Compiling and installing ``omni``
=================================

To compile and install ``omni``, you have two options:

1. Install the ``sdist`` package from PyPI.

You can install the ``sdist`` package from PyPI by running:

.. code-block:: bash
    
    # installs the sdist package from PyPI
    pip install omnineuro --no-binary omnineuro

2. Install the package from the ``git`` repo.

This involves cloning the git repo and using ``pip`` to install the package:

.. code-block:: bash

    # clone the git repo
    git clone https://gitlab.com/vanandrew/omni.git

    # install omni package
    pip install [path/to/the/cloned/omni/directory]

If you are running into compilation issues, it is helpful to use ``pip``'s verbose option ``-v``
to see what is happening.

Running ``omni`` on Docker
**************************

It is also possible to run ``omni`` on Docker. A pre-built Docker image can be found
at `DockerHub <https://hub.docker.com/r/vanandrew/omni>`_.

.. code-block:: bash

    # The first argument to the docker image is the command to run
    # Note that unlike the local install, commands are called without the
    # ``omni_`` prefix.
    docker run -it --rm vanandrew/omni affine -h

    # Running the image without any arguments will give you a list of
    # available commands.
    docker run -it --rm vanandrew/omni

For Developers
**************

If you are a developer, it may be convenient to install ``omni`` in editable mode for making code modifications and
testing. This will allow you to directly work out of the cloned git repo (instead of modifying files in whatever
location ``pip`` installs ``omni`` into).

You will need to follow all the same steps as in :ref:`omni-source-build`.

.. code-block:: bash

    git clone https://gitlab.com/vanandrew/omni.git
    pip install -e [path/to/the/cloned/omni/directory]


What's next?
************

After downloading and installing, you may want to read the :ref:`omni_pipeline_walkthrough`.
Over the course of that tutorial, you learn how to test and validate the success of your installation by using the
script, ``omni_pipeline`` to preprocess the included example data set. Reading through this section will help you
understand how ``omni`` preprocessing scripts organize their outputs, the purposes of the various intermediary files
they produce, and give you a sense of where to begin when troubleshooting registration issues, if they happen to crop
up.

.. _FSL: https://fsl.fmrib.ox.ac.uk/fsl/fslwiki

.. _AFNI: https://afni.nimh.nih.gov/

.. _ANTs: http://stnava.github.io/ANTs/
