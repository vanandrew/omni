**omni_pipeline**
=================

Overview
--------

Example commands
----------------

.. code-block:: shell

	# Example 1
	omni_pipeline [Dataset] [Output]

Argument types
----------------

	:ref:`pipeline-required-arguments-label`.
	
	:ref:`pipeline-optional-arguments-label`.
	
		:ref:`pipeline-debiasing-arguments-label`.
		
		:ref:`pipeline-brain-mask-arguments-label`.

		:ref:`pipeline-moco-arguments-label`.

		:ref:`pipeline-synthtarget-arguments-label`.

		:ref:`pipeline-distortion-correction-arguments-label`.
		
		:ref:`pipeline-noise-mask-arguments-label`.

		:ref:`pipeline-atlas-arguments-label`.

		:ref:`pipeline-other-arguments-label`.

.. _pipeline-required-arguments-label:

Required arguments
------------------

.. add_argument:: omni.scripts.pipeline bids_path

.. add_argument:: omni.scripts.pipeline output_path

.. _pipeline-optional-arguments-label:

Optional arguments
------------------

.. add_argument:: omni.scripts.pipeline participant_label

.. add_argument:: omni.scripts.pipeline dryrun

.. add_argument:: omni.scripts.pipeline single_rest_run

.. add_argument:: omni.scripts.pipeline combine_sessions

.. add_argument:: omni.scripts.pipeline skip_validation

.. add_argument:: omni.scripts.pipeline database

.. add_argument:: omni.scripts.pipeline reset_database

.. add_argument:: omni.scripts.pipeline skip_database

.. add_argument:: omni.scripts.pipeline log_file

.. add_argument:: omni.scripts.pipeline program

.. add_argument:: omni.scripts.pipeline dilation_size

.. add_argument:: omni.scripts.pipeline ref

.. _pipeline-debiasing-arguments-label:

Image debiasing options
^^^^^^^^^^^^^^^^^^^^^^^

.. note::
	Intensity bias fields affecting anatomical T1w/T2w images or the EPI images
	can significantly and negatively affect the quality of synthetic EPI images
	created to correct EPI distortion using the *Synth* field map-less approach. It is
	very important that bias fields be removed effectively, at least during the
	registration and distortion correction process. The following parameters can be
	used to tailor the BSpline model used to estimate and remove intensity bias.

	Because creating synthetic EPI images relies on finding a mapping between voxel
	intensities of T1w/T2w source images and a EPI target image, any intensity bias
	remaining in the images can reduce the accuracy of the synthetic image. Be
	mindful that over fitting the bias field can also mess up the intensity mapping
	relationship. Over fitting is most likely to occur by allowing too many knots in
	the spline model, or placing them too closely together. The telltail sign that
	this has occurred is when the debiased images look like they have been highpass
	filtered, like they lack all low spatial frequency information. If this happens,
	try increasing the distance between spline knots (parameterized by the first
	number in the debais_params_anat and debias_params_func strings). Typing
	``N4BiasFieldCorrection -h`` in the command line is also going to be very helpful.

.. add_argument:: omni.scripts.synthpreproc debias_params_anat

.. add_argument:: omni.scripts.synthpreproc debias_params_func

.. _pipeline-brain-mask-arguments-label:

Binary brain mask options
^^^^^^^^^^^^^^^^^^^^^^^^^

.. note::
	``omni_pipeline`` makes heavy use of binary brain masks at multiple stages
	during preprocessing. For instance, a binary anatomical brain mask aligned to
	the EPI images plays a key role as an initial estimate for which voxels contain
	valid signal and which ones are likely to contain noise when constructing
	signal-to-noise masks. It also contributes to a weight volume that increases the
	importance of brain voxels when creating the synthetic EPI image used in field
	map-less distortion correction.

.. add_argument:: omni.scripts.pipeline bet_method

.. note::
	We have observed in some data sets (particularly for subjects with large
	lesions) ``BET`` masks based on the T1w image can exhibit significant errors.
	Accuracy is much improved when the composite image produced by enabling the
	'Norm' option are used as inputs to ``BET``. In our test, this approach is quite
	reliable, but if your brain masks look odd, try switching back to the ``T1`` option.

.. add_argument:: omni.scripts.pipeline fractional_intensity_threshold_anat

.. add_argument:: omni.scripts.pipeline fractional_intensity_threshold_func

.. _pipeline-moco-arguments-label:

Simultaneous slice time and motion correction options
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. note::
	``omni_pipeline`` can use either ``3dAllineate`` or ``SpaceTimeRealign`` for
	motion correction.
	
	The SpaceTimeRealign_ algorithm to implement
	simultaneous  slice time and motion correction. Corrections produced by this
	algorithm appear to be very high quality, but in its present single-threaded
	form it can be quite time consuming to run. The following options allow the user
	to tailor the performance of the SpaceTimeRealign_ algorithm to suit their
	tolerance for delay and thumb-twiddling.

	SpaceTimeRealign_ aligns frames using a subsample pyramid resolution scheme,
	performing coarse alignment at low resolution and fine alignment at high
	resolution. At each resolution level the alignments can be refined over a
	variable number of 'loops'. Processing time can be reduced by decreasing the
	number of refining loops for each resolution level or choosing a coarser highest
	resolution.

	Example: ``omni_pipeline ... --subsample 5 3 2 --loops 3 2 1 ....`` would
	perform three alignment refining iterations at 5x subsample; two iterations at
	3x subsample; and one iteration at 1x sampling resolution (native EPI
	resolution)

.. add_argument:: omni.scripts.pipeline moco

.. add_argument:: omni.scripts.pipeline loops

.. add_argument:: omni.scripts.pipeline subsample

.. add_argument:: omni.scripts.pipeline borders

.. _pipeline-synthtarget-arguments-label:

Synthtarget options
^^^^^^^^^^^^^^^^^^^

.. note::
	``omni_pipeline`` corrects EPI image distortion by non-linearly warping
	distorted EPI images to align with a high resolution undistorted synthetic EPI
	image. Creation of the synthetic image is accomplished by the standalone utility
	omni_synthtarget. The followiing options modify the action of the internal calls
	to`` omni_synthtarget``.

.. add_argument:: omni.scripts.pipeline initial_synth_model

.. add_argument:: omni.scripts.pipeline final_synth_model

.. add_argument:: omni.scripts.pipeline bandwidth

.. add_argument:: omni.scripts.pipeline skip_affine

.. add_argument:: omni.scripts.pipeline skip_synthtarget_affine

.. add_argument:: omni.scripts.pipeline resolution_pyramid

.. add_argument:: omni.scripts.pipeline synthtarget_max_iterations

.. add_argument:: omni.scripts.pipeline synthtarget_err_tol

.. add_argument:: omni.scripts.pipeline synthtarget_step_size

.. _pipeline-distortion-correction-arguments-label:

Distortion correction options
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. add_argument:: omni.scripts.pipeline resample_resolution

.. add_argument:: omni.scripts.pipeline sigma_t2

.. add_argument:: omni.scripts.pipeline distortion_correction_smoothing

.. add_argument:: omni.scripts.pipeline distortion_correction_shrink_factors

.. add_argument:: omni.scripts.pipeline distortion_correction_step_size

.. add_argument:: omni.scripts.pipeline warp_direction

.. _pipeline-noise-mask-arguments-label:

Noise mask options
^^^^^^^^^^^^^^^^^^

.. note::
	Properly detecting regions of the EPI data set that are primarily noise
	(due to signal drop out) is a very important step in generating high quality
	synthetic EPI image used for distortion correction. The following options allow
	the user to fine tune certain aspects of the noise region detection process and
	the resulting noise region weight masks that reduce the contribution of noisy
	regions when estimating the synthetic EPI image. If the synthetic EPI images
	produced by omni_pipeline  are do not appear to you eye to be sufficiently
	similar to the real EPI data that you are tying to correct, adjusting these
	parameters may improve things for you.

.. add_argument:: omni.scripts.pipeline noise_mask_iterations
	
.. add_argument:: omni.scripts.pipeline noise_mask_dilation_size

.. add_argument:: omni.scripts.pipeline noise_mask_sigma

.. _pipeline-atlas-arguments-label:

Atlas options
^^^^^^^^^^^^^

.. add_argument:: omni.scripts.pipeline data_resolution

.. add_argument:: omni.scripts.pipeline atlas

.. add_argument:: omni.scripts.pipeline atlas_label

.. _pipeline-other-arguments-label:

Other options
^^^^^^^^^^^^^

.. add_argument:: omni.scripts.pipeline config_file

.. add_argument:: omni.scripts.pipeline generate_config

.. add_argument:: omni.scripts.pipeline number_of_threads

.. add_argument:: omni.scripts.pipeline version

.. add_argument:: omni.scripts.pipeline help


.. _SpaceTimeRealign: http://nipy.org/nipy/api/generated/nipy.algorithms.registration.groupwise_registration.html#spacetimerealign
