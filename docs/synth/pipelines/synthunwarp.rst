**omni_synthunwarp**
====================

Overview
--------

Example commands
----------------

.. code-block:: shell

	omni_synthunwarp

Argument types
----------------

In order to run, ``omni_synthunwarp`` requires 

	:ref:`synthunwarp-required-arguments-label`.
	
	:ref:`synthunwarp-optional-arguments-label`.
	
		:ref:`synthunwarp-synthtarget-arguments-label`.

		:ref:`synthunwarp-distortion-correction-arguments-label`.
		
		:ref:`synthunwarp-noise-mask-arguments-label`.

		:ref:`synthunwarp-other-arguments-label`.

.. _synthunwarp-required-arguments-label:

Required arguments
------------------

.. add_argument:: omni.scripts.synthunwarp output_path

.. add_argument:: omni.scripts.synthunwarp t1_debias

.. add_argument:: omni.scripts.synthunwarp t2_debias

.. add_argument:: omni.scripts.synthunwarp anat_bet_mask

.. add_argument:: omni.scripts.synthunwarp ref_epi

.. add_argument:: omni.scripts.synthunwarp ref_epi_bet_mask

.. add_argument:: omni.scripts.synthunwarp epi

.. _synthunwarp-optional-arguments-label:

Optional arguments
------------------

.. add_argument:: omni.scripts.synthunwarp anat_eye_mask

.. add_argument:: omni.scripts.synthunwarp program

.. add_argument:: omni.scripts.synthunwarp dilation_size

.. _synthunwarp-synthtarget-arguments-label:

Synthtarget options
^^^^^^^^^^^^^^^^^^^

.. note::
	``omni_synthunwarp`` corrects EPI image distortion by non-linearly warping
	distorted EPI images to align with a high resolution undistorted synthetic EPI
	image. Creation of the synthetic image is accomplished by the standalone utility
	omni_synthtarget. The followiing options modify the action of the internal calls
	to`` omni_synthtarget``.

.. add_argument:: omni.scripts.synthunwarp initial_synth_model

.. add_argument:: omni.scripts.synthunwarp final_synth_model

.. add_argument:: omni.scripts.synthunwarp bandwidth

.. add_argument:: omni.scripts.synthunwarp skip_affine

.. add_argument:: omni.scripts.synthunwarp skip_synthtarget_affine

.. add_argument:: omni.scripts.synthunwarp resolution_pyramid

.. add_argument:: omni.scripts.synthunwarp synthtarget_max_iterations

.. add_argument:: omni.scripts.synthunwarp synthtarget_err_tol

.. add_argument:: omni.scripts.synthunwarp synthtarget_step_size

.. _synthunwarp-distortion-correction-arguments-label:

Distortion correction options
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. add_argument:: omni.scripts.synthunwarp resample_resolution

.. add_argument:: omni.scripts.synthunwarp sigma_t2

.. add_argument:: omni.scripts.synthunwarp initial_warp_field

.. add_argument:: omni.scripts.synthunwarp distortion_correction_smoothing

.. add_argument:: omni.scripts.synthunwarp distortion_correction_shrink_factors

.. add_argument:: omni.scripts.synthunwarp distortion_correction_step_size

.. add_argument:: omni.scripts.synthunwarp warp_direction

.. _synthunwarp-noise-mask-arguments-label:

Noise mask options
^^^^^^^^^^^^^^^^^^

.. note::
	Properly detecting regions of the EPI data set that are primarily noise
	(due to signal drop out) is a very important step in generating high quality
	synthetic EPI image used for distortion correction. The following options allow
	the user to fine tune certain aspects of the noise region detection process and
	the resulting noise region weight masks that reduce the contribution of noisy
	regions when estimating the synthetic EPI image. If the synthetic EPI images
	produced by omni_synthunwarp are do not appear to you eye to be sufficiently
	similar to the real EPI data that you are tying to correct, adjusting these
	parameters may improve things for you.

.. add_argument:: omni.scripts.synthunwarp noise_mask_iterations
	
.. add_argument:: omni.scripts.synthunwarp noise_mask_dilation_size

.. add_argument:: omni.scripts.synthunwarp noise_mask_sigma

.. _synthunwarp-other-arguments-label:

Other options
^^^^^^^^^^^^^

.. add_argument:: omni.scripts.synthpreproc config_file

.. add_argument:: omni.scripts.synthpreproc generate_config

.. add_argument:: omni.scripts.synthunwarp number_of_threads

.. add_argument:: omni.scripts.synthunwarp version

.. add_argument:: omni.scripts.synthunwarp help


.. _SpaceTimeRealign: http://nipy.org/nipy/api/generated/nipy.algorithms.registration.groupwise_registration.html#spacetimerealign
