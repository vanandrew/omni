**omni_synthpreproc**
=====================

Overview
--------
``omni_synthpreproc`` is an fMRI pre-processing script that implements
*Synth* field map-less distortion correction. It can be used to perform high
quality image registration and data preparation, or it can serve as a reference
implementation for research groups who wish to incorporate *Synth* into their own
bespoke processing pipelines.

``omni_synthpreproc`` performs the following operations:

#. Co-registration of T1w and T2w anatomical images
#. Estimation and removal of intensity bias fields from T1w, T2w and reference EPI data
#. Simultaneous slice time and motion correction of (functional/diffusion) EPI data sets
#. Creation of synthetic EPI image, using omni_synthtarget
#. Field map-less distortion correction and alignment to anatomical T1w/T2w images using omni_synthunwarp 
#. Final alignment of T1w, T2w and EPI data to the MNI atlas space

Example commands
----------------

.. code-block:: shell

	# Example 1
	omni_synthpreproc -o ~/data/output --t1 ./T1w.nii.gz --t2 ./T2w.nii.gz \
	--func ./TaskData.nii.gz --bids_sidecar ./BIDsInfo.json

Argument types
----------------

In order to run, ``omni_synthpreproc`` requires a minimum of five non-positional
input arguments. It also supports many optional arguments to fine tune
preprocessing performance to your liking.

	:ref:`synthpreproc-required-arguments-label`.
	
	:ref:`synthpreproc-optional-arguments-label`.
	
		:ref:`synthpreproc-debiasing-arguments-label`.
		
		:ref:`synthpreproc-brain-mask-arguments-label`.

		:ref:`synthpreproc-moco-arguments-label`.

		:ref:`synthpreproc-synthtarget-arguments-label`.

		:ref:`synthpreproc-distortion-correction-arguments-label`.
		
		:ref:`synthpreproc-noise-mask-arguments-label`.

		:ref:`synthpreproc-atlas-arguments-label`.

		:ref:`synthpreproc-other-arguments-label`.

.. _synthpreproc-required-arguments-label:

Required arguments
------------------

.. add_argument:: omni.scripts.synthpreproc output_path

.. add_argument:: omni.scripts.synthpreproc t1

.. add_argument:: omni.scripts.synthpreproc t2

.. add_argument:: omni.scripts.synthpreproc func

.. add_argument:: omni.scripts.synthpreproc bids_sidecar

.. note::
	In a pinch, the user can create their own minimal JSON file manually.
	Currently, the required fields are: "SliceTiming", "PhaseEncodingDirection",
	"DwellTime", and "RepetitionTime".

.. _synthpreproc-optional-arguments-label:

Optional arguments
------------------

.. add_argument:: omni.scripts.synthpreproc log_file

.. add_argument:: omni.scripts.synthpreproc ref

.. add_argument:: omni.scripts.synthpreproc program

.. add_argument:: omni.scripts.synthpreproc dilation_size

.. _synthpreproc-debiasing-arguments-label:

Image debiasing options
^^^^^^^^^^^^^^^^^^^^^^^

.. note::
	Intensity bias fields affecting anatomical T1w/T2w images or the EPI images
	can significantly and negatively affect the quality of synthetic EPI images
	created to correct EPI distortion using the *Synth* field map-less approach. It is
	very important that bias fields be removed effectively, at least during the
	registration and distortion correction process. The following parameters can be
	used to tailor the BSpline model used to estimate and remove intensity bias.

	Because creating synthetic EPI images relies on finding a mapping between voxel
	intensities of T1w/T2w source images and a EPI target image, any intensity bias
	remaining in the images can reduce the accuracy of the synthetic image. Be
	mindful that over fitting the bias field can also mess up the intensity mapping
	relationship. Over fitting is most likely to occur by allowing too many knots in
	the spline model, or placing them too closely together. The telltail sign that
	this has occurred is when the debiased images look like they have been highpass
	filtered, like they lack all low spatial frequency information. If this happens,
	try increasing the distance between spline knots (parameterized by the first
	number in the debais_params_anat and debias_params_func strings). Typing
	``N4BiasFieldCorrection -h`` in the command line is also going to be very helpful.

.. add_argument:: omni.scripts.synthpreproc debias_params_anat

.. add_argument:: omni.scripts.synthpreproc debias_params_func

.. _synthpreproc-brain-mask-arguments-label:

Binary brain mask options
^^^^^^^^^^^^^^^^^^^^^^^^^

.. note::
	``omni_synthpreproc`` makes heavy use of binary brain masks at multiple stages
	during preprocessing. For instance, a binary anatomical brain mask aligned to
	the EPI images plays a key role as an initial estimate for which voxels contain
	valid signal and which ones are likely to contain noise when constructing
	signal-to-noise masks. It also contributes to a weight volume that increases the
	importance of brain voxels when creating the synthetic EPI image used in field
	map-less distortion correction.

.. add_argument:: omni.scripts.synthpreproc bet_method

.. note::
	We have observed in some data sets (particularly for subjects with large
	lesions) ``BET`` masks based on the T1w image can exhibit significant errors.
	Accuracy is much improved when the composite image produced by enabling the
	'Norm' option are used as inputs to ``BET``. In our test, this approach is quite
	reliable, but if your brain masks look odd, try switching back to the ``T1`` option.

.. add_argument:: omni.scripts.synthpreproc fractional_intensity_threshold_anat

.. add_argument:: omni.scripts.synthpreproc fractional_intensity_threshold_func

.. _synthpreproc-moco-arguments-label:

Simultaneous slice time and motion correction options
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. note::
	``omni_synthpreproc`` uses the SpaceTimeRealign_ algorithm to implement
	simultaneous  slice time and motion correction. Corrections produced by this
	algorithm appear to be very high quality, but in its present single-threaded
	form it can be quite time consuming to run. The following options allow the user
	to tailor the performance of the SpaceTimeRealign_ algorithm to suit their
	tolerance for delay and thumb-twiddling.

	SpaceTimeRealign_ aligns frames using a subsample pyramid resolution scheme,
	performing coarse alignment at low resolution and fine alignment at high
	resolution. At each resolution level the alignments can be refined over a
	variable number of 'loops'. Processing time can be reduced by decreasing the
	number of refining loops for each resolution level or choosing a coarser highest
	resolution.

	Example: ``omni_synthpreproc ... --subsample 5 3 2 --loops 3 2 1 ....`` would
	perform three alignment refining iterations at 5x subsample; two iterations at
	3x subsample; and one iteration at 1x sampling resolution (native EPI
	resolution)

.. add_argument:: omni.scripts.synthpreproc moco

.. add_argument:: omni.scripts.synthpreproc loops

.. add_argument:: omni.scripts.synthpreproc subsample

.. add_argument:: omni.scripts.synthpreproc borders

.. _synthpreproc-synthtarget-arguments-label:

Synthtarget options
^^^^^^^^^^^^^^^^^^^

.. note::
	``omni_synthpreproc`` corrects EPI image distortion by non-linearly warping
	distorted EPI images to align with a high resolution undistorted synthetic EPI
	image. Creation of the synthetic image is accomplished by the standalone utility
	omni_synthtarget. The followiing options modify the action of the internal calls
	to`` omni_synthtarget``.

.. add_argument:: omni.scripts.synthpreproc initial_synth_model

.. add_argument:: omni.scripts.synthpreproc final_synth_model

.. add_argument:: omni.scripts.synthpreproc bandwidth

.. add_argument:: omni.scripts.synthpreproc skip_affine

.. add_argument:: omni.scripts.synthpreproc skip_synthtarget_affine

.. add_argument:: omni.scripts.synthpreproc resolution_pyramid

.. add_argument:: omni.scripts.synthpreproc synthtarget_max_iterations

.. add_argument:: omni.scripts.synthpreproc synthtarget_err_tol

.. add_argument:: omni.scripts.synthpreproc synthtarget_step_size

.. _synthpreproc-distortion-correction-arguments-label:

Distortion correction options
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. add_argument:: omni.scripts.synthpreproc resample_resolution

.. add_argument:: omni.scripts.synthpreproc sigma_t2

.. add_argument:: omni.scripts.synthpreproc distortion_correction_smoothing

.. add_argument:: omni.scripts.synthpreproc distortion_correction_shrink_factors

.. add_argument:: omni.scripts.synthpreproc distortion_correction_step_size

.. add_argument:: omni.scripts.synthpreproc warp_direction

.. _synthpreproc-noise-mask-arguments-label:

Noise mask options
^^^^^^^^^^^^^^^^^^

.. note::
	Properly detecting regions of the EPI data set that are primarily noise
	(due to signal drop out) is a very important step in generating high quality
	synthetic EPI image used for distortion correction. The following options allow
	the user to fine tune certain aspects of the noise region detection process and
	the resulting noise region weight masks that reduce the contribution of noisy
	regions when estimating the synthetic EPI image. If the synthetic EPI images
	produced by omni_synthpreproc are do not appear to you eye to be sufficiently
	similar to the real EPI data that you are tying to correct, adjusting these
	parameters may improve things for you.

.. add_argument:: omni.scripts.synthpreproc noise_mask_iterations
	
.. add_argument:: omni.scripts.synthpreproc noise_mask_dilation_size

.. add_argument:: omni.scripts.synthpreproc noise_mask_sigma

.. _synthpreproc-atlas-arguments-label:

Atlas options
^^^^^^^^^^^^^

.. add_argument:: omni.scripts.synthpreproc data_resolution

.. add_argument:: omni.scripts.synthpreproc atlas

.. add_argument:: omni.scripts.synthpreproc atlas_label

.. _synthpreproc-other-arguments-label:

Other options
^^^^^^^^^^^^^

.. add_argument:: omni.scripts.synthpreproc config_file

.. add_argument:: omni.scripts.synthpreproc generate_config

.. add_argument:: omni.scripts.synthpreproc number_of_threads

.. add_argument:: omni.scripts.synthpreproc version

.. add_argument:: omni.scripts.synthpreproc help


.. _SpaceTimeRealign: http://nipy.org/nipy/api/generated/nipy.algorithms.registration.groupwise_registration.html#spacetimerealign
