**omni_synthnoisemask**
=======================

Overview
--------
``omni_synthnoisemask`` is a utility that is used to automatically detect and
label regions of an EPI data set that are likely to be affected by unacceptable
levels of signal drop out and noise. The binary masks produced by this utility
are useful for a variety of purposes, however it was designed initially to
improve the quality of synthetic EPI images used for field map-less distortion
correction by reducing the contribution of highly distorted areas when
estimating the voxel intensity mapping between source images (e.g., undistorted
T1w and T2w images) and a target image (e.g., a low resolution distorted EPI).

.. note::

	``omni_synthnoisemask`` creates noise mask using the following procedure: An
	affine aligned anatomical brain mask is used as an initial signal and noise
	labeling. All voxels in this this mask labeled with ones are initially
	considered to be "signal"; all voxels labeled with zeros are considered to
	be "noise". A between-class linear discriminant analysis (LDA) is run on the
	raw (un-centered and un-scaled) EPI time series for all of the voxels (this
	is why it is important that frame-wise alignement and slice time correction
	be performed *first*). Each voxel's time series is then projected onto the
	principal discriminant vector. The procedure is repeated, this time using
	the centered and normalized voxel time series. The centered and normalized
	time voxel time series are projected onto the new principal discriminant
	vector. Each voxel at this point is associated with two projection values. A
	third value is created for each voxel equal to the product of the two LDA
	projection values. A second level LDA is then performed using these three
	values and each voxel's triplet of values is projected onto the resulting
	principal discriminant vector. Otsu's method is then used to classify each
	voxel as signal or noise depending on its projection onto this vector and
	produce an updated label for each voxel. This process is then repeated.
	Sufficiently iterated the signal and noise labels produced by this method
	converge and stabilize. 

 

Example commands
----------------
.. code-block:: shell
	
	# Example 1: 
	omni_synthnoisemask ~/data/EPIData.nii.gz ~/data/AlignedAnatomicalBETMask.nii.gz ~/data/EPINoiseMask.nii.gz

	# Example 2: 
	omni_synthnoisemask ~/data/EPIData.nii.gz ~/data/AlignedAnatomicalBETMask.nii.gz ~/data/EPINoiseMask.nii.gz -d 3 -k 8


Illustration
------------
.. figure:: ../../_static/figures/omni_synthnoisemask_figure_01.png

	**Initial anatomical brain mask and resulting noise mask** Left: the anatomically derived brain mask (red) is displayed as an overlay on the subject's T1w image. Right: the brain mask pre-aligned to the EPI data data set. The pre-aligned mask indicates the relevant region of in which to detect noise and is an initial labeling of potentially valid voxels. The resulting binary noise mask (yellow) produced after 12 iterations.

Arguments
---------

Required arguments
++++++++++++++++++

.. add_argument:: omni.scripts.synthnoisemask epi

.. add_argument:: omni.scripts.synthnoisemask anat_bet_mask_epispace

.. add_argument:: omni.scripts.synthnoisemask output

Optional arguments
++++++++++++++++++

.. add_argument:: omni.scripts.synthnoisemask noise_mask_iterations

.. add_argument:: omni.scripts.synthnoisemask noise_mask_dilation_size

.. add_argument:: omni.scripts.synthnoisemask noise_mask_sigma

.. add_argument:: omni.scripts.synthnoisemask version

.. add_argument:: omni.scripts.synthnoisemask help

