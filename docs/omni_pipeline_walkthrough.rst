.. _omni_pipeline_walkthrough:

Walkthrough: Field map-lessly correcting distortion in an example data set
==========================================================================

Getting started
---------------
So, you've `downloaded <https://gitlab.com/vanandrew/omni/>`_ and :ref:`installed <installation>` the ``omni`` neuroimaging registration package with ``synth`` and would now like to perform some field map-less distortion correction on your neuromimaging data. First, we'll walk you through how to validate the success of your installation on the sample data set included with your download. In addition to making sure that everything is running correctly on your system, this will give us the opportunity to discuss the way that the ``omni`` preprocessing pipelines organizes data. This will be important if you'd like to know the output location of your preprocessed data so that you can begin doing some analyses. It will also be helpful for understanding the various files that are produced so that you can effectively troubleshoot difficult registration cases, or understand how adjusting the many preprocessing parameters available in the ``synth`` preprocessing pipelines affect different aspects of your data.

.. note::
    
    This tutorial will use ``omni_pipeline``, neuroimaging script which requires that the input data be organized in `BIDS`_ format. For data sets that are not organized in `BIDS`_ format, the preprocessing script, ``omni_synthpreproc``, is available. However, that script requires more input from the the user in order to operate. Organizing data in `BIDS`_ format is highly recommended. A utility like `dcm2niix`_ is very helpful for this.

Introduction to the example BIDS data set
-----------------------------------------

The ``omni`` neuroimaging registration package comes with a minimal data set that's used to validate successful installation and for demonstration purposes. This section acts as a quick introduction to the `BIDS`_ organization structure relied on by ``omni_pipeline``. Users with a working familiarity of `BIDS`_ may want to skip to the next section.

To begin, first navigate to where you've installed ``omni``. In this example, ``omni`` has been installed here:

.. code-block:: bash
    
    pwd
    ~/Projects/omni

The example data set packaged with omni is stored here:

.. code-block:: bash

    ls ~/Projects/omni/tests/test_data/dataset
    total 0
    drwxr-xr-x  8 montez  staff  256 Apr 19 13:50 test_bids

As the directory name, ``test_bids``, suggests, this example data set is in `BIDS`_ format.

A quick look inside the the root ``test_bids`` using ``find`` to recursively list the subdirectories will give you a sense of what the required data structure is.

.. code-block:: bash

    find ./tests/test_data/dataset/test_bids -type d
    ./tests/test_data/dataset/test_bids
    ./tests/test_data/dataset/test_bids/sub-test01
    ./tests/test_data/dataset/test_bids/sub-test01/ses-test01
    ./tests/test_data/dataset/test_bids/sub-test01/ses-test01/anat
    ./tests/test_data/dataset/test_bids/sub-test01/ses-test01/func

From this output we can see that this example data set contains, one subject, ``sub-test01`` with one session, ``ses-test01``. 

The root directory for the study contains a key file that is necessary for ``omni_pipeline`` to operate:

.. code-block:: bash

    ls ./tests/test_data/dataset/test_bids/
    total 40
    -rw-r--r--  1 montez  staff    47 Apr 19 13:50 README
    -rw-r--r--  1 montez  staff   627 Apr 19 13:50 T1w.json
    -rw-r--r--  1 montez  staff   602 Apr 19 13:50 T2w.json
    -rw-r--r--  1 montez  staff   132 Apr 19 13:50 dataset_description.json
    drwxr-xr-x  3 montez  staff    96 Apr 19 13:50 sub-test01
    -rw-r--r--  1 montez  staff  1482 Apr 19 13:50 task-rest_bold.json

Aside from the actual data files contained within the ``sub-test01`` directory, the most important file here is ``task-rest_bold.json``, which stores acquition details such as TR and slice time information.

The data set for this subject consists of high resolution T1w and T2w anatomical images, located in the ``anat`` directory:

.. code-block:: bash

    ls ./tests/test_data/dataset/test_bids/sub-test01/ses-test01/anat
    -rw-r--r--  1 montez  staff  18659682 Apr 19 13:50 sub-test01_ses-test01_T1w.nii.gz
    -rw-r--r--  1 montez  staff  15952219 Apr 19 13:50 sub-test01_ses-test01_T2w.nii.gz


The second part of the data set is a set of four separate resting state scans acquired during the scan session. The relevant files here are stored in the ``func`` sub-directory:

.. code-block:: bash   

    ls ./tests/test_data/dataset/test_bids/sub-test01/ses-test01/func

        total 13184
    -rw-r--r--  1 montez  staff  1682344 Apr 19 13:50 sub-test01_ses-test01_task-rest_run-01_bold.nii.gz
    -rw-r--r--  1 montez  staff  1684141 Apr 19 13:50 sub-test01_ses-test01_task-rest_run-02_bold.nii.gz
    -rw-r--r--  1 montez  staff  1687398 Apr 19 13:50 sub-test01_ses-test01_task-rest_run-03_bold.nii.gz
    -rw-r--r--  1 montez  staff  1691601 Apr 19 13:50 sub-test01_ses-test01_task-rest_run-04_bold.nii.gz

In order to speed pre-processing for the purposes of illustration, the supplied  data set is relatively low resolution (4mm isotropic voxels) and consist of only 11 frames of data per scan.

Running ``omni_pipeline`` on the example BIDS data set
------------------------------------------------------
To test your installation of ``omni_pipeline`` on the supplied sample data set perform the following steps:
    
    #. Navigate to your omni installation directory (represented here by the variable, ``$OMNIDIR``). 
    
    #. Create a directory (represented by the variable, ``$OUTPUTDIR``) to store the final preprocessed data and intermediary files using the command ``mkdir $OUTPUTDIR``
    
    #. Now, run ``omni_pipeline`` on the test data set with the following command:

    .. code-block:: bash 

        omni_pipeline $OMNIDIR/tests/test_data/dataset/test_bids $OUTPUTDIR --resample_resolution 1.5

    .. note::
        
        The first positional command line argument points to the root directory of a `BIDS`_ data set. The second posiltional argument points to an output directory for the preprocessed data. In this example, the final optional named argument, ``--resample_resolution 1.5`` refers to the resolution (length of a voxel edge in mm) at which the distortion correcting warp will be estimated. Typically, this option is left at the default value and is determined by the native resolution of the T1w and T2w images (in this case, 1 mm). For the sake of expedience, we'll resample the anatomical images to a slighly lower resolution for this example, athough doing this may have some small negative effects on the accuracy of the distortion correction.

    If all goes well, you should begin to see status log output that looks something like this on your console:

    .. code-block:: bash 

        [2021-04-20 17:14:05,193] INFO: Namespace(atlas='mni', bandwidth=16, bet_method='Norm', bids_path='./tests/test_data/dataset/test_bids', combine_sessions=False, data_resolution=None, database=None, debias_params_anat='[100,3,1x1x1,3]', debias_params_func='[200,3,3x3x3,3]', distortion_correction_step_size=[3, 1, 0.1], dryrun=False, final_synth_model='rbf(0;12)+rbf(1;12)+rbf(0;12)*rbf(1;12)', fractional_intensity_threshold_anat=0.5, fractional_intensity_threshold_func=0.5, initial_synth_model='rbf(0;4)+rbf(1;4)+rbf(0;4)*rbf(1;4)', log_file=None, loops=[1, 1, 1], noise_mask_dilation_size=2, noise_mask_iterations=12, noise_mask_sigma=2, number_of_threads=8, output_path='/data/nil-bluearc/GMT/David/omni_testdataset', participant_label=None, program='fsl', resample_resolution=1.5, reset_database=False, resolution_pyramid=[4, 2, 1], sigma_t2=0.5, skip_affine=False, skip_database=False, skip_validation=False, subsample=[5, 3, 1], synthtarget_err_tol=[0.0001, 0.0001, 0.0005], synthtarget_max_iterations=[2000, 500, 100], synthtarget_step_size=[0.001, 0.001, 0.001])
        [2021-04-20 17:14:05,193] INFO: 
        [2021-04-20 17:14:05,486] INFO: Subjects in dataset: ['test01']
        [2021-04-20 17:14:05,493] INFO: Found these sessions for subject test01: ['test01']
        [2021-04-20 17:14:05,513] INFO: anat_path: sub-test01/ses-test01/anat/sub-test01_ses-test01
        [2021-04-20 17:14:05,513] INFO: func_path: sub-test01/ses-test01/func/sub-test01_ses-test01_task-rest_run-01_bold
        [2021-04-20 17:14:05,513] INFO: epi_path: sub-test01/ses-test01/func/sub-test01_ses-test01_task-rest_run-01_bold
        [2021-04-20 17:14:05,516] INFO: Changed working directory to: /data/nil-bluearc/GMT/David/omni_testdataset/sub-test01/ses-test01/anat/sub-test01_ses-test01
        [2021-04-20 17:14:05,611] INFO: Loading cached results...
        [2021-04-20 17:14:05,715] INFO: Skipping stage: deoblique_anat execution...
        [2021-04-20 17:14:05,715] INFO: Loading cached results...
        [2021-04-20 17:14:05,814] INFO: Loading cached results...
        ...

The status log output informs you in detail about what successes (or failures) of each stage of the preprocessing pipeline. Examining this output is the first place to begin if you encounter any problems running ``omni_pipeline`` or ``omni_synthpreproc``.

The first line of this file lists the values of each of the parameters use in the various stages of preprocessing. In this case, because we only provided one optional argument, (``--resample_resolution 1.5``) default values were used to populate the parameters for preprocessing the example data set. In our experiments, these parameters have worked pretty well across a range of data sets. Your mileage may vary though. If you find that different parameters seem to work better for you, consider dropping us a note on the `omni gitlab issues`_ page, with some information about your data set and the parameters you used. We will update the default parameters to those that work the best for most people.

.. note::
    
    By default the status log is printed to the console screen. To save the status log to a text file for posterity, use the ``--log_file $LOGFILENAME`` optional argument. This will save the status log in its entirety to the file, $LOGFILENAME.

Exploring ``omni_pipeline`` output
----------------------------------

By the time ``omni_pipeline`` has finished running it will have created an organized directory structure within the ``$OUTPUT`` directory that you have selected. This directory structure contains the final data, aligned and registered to your selected atlas, as well as many intermediary files. In this section we'll look in detail at these output files.

.. note::

    If you followed this walkthrough on your own `BIDS`_ data set and just want to know where your registered data is located so you can get started, you'll find it here:

    Atlas aligned anatomical data: ``$OUTPUTDIR/sub-$SUBJ/ses-$SESSION/anat/sub-$SUBJ_ses-$SESSION/anat_proc_4_align_atlas``

    Atlas aligned task or rest data: ``$OUTPUTDIR/sub-$SUBJ/ses-$SESSION/func/sub-$SUBJ_ses-$SESSION_task-$TASK_run-$RUN/epi_proc_2_combine_transforms/``


.. note::

    Within each of the intermediary output directories prefixed with ``anat_proc``, ``func_proc``, or ``epi_proc``, you will find small text files suffixed with ``.input``, ``.output``, and ``.stage``. These files are used by ``omni_pipeline`` and ``omni_preproc`` to efficiently manage the process of re-processing data after if the user alters any preprocessing parameters. They assist in determing which stages of preprocessing actually need to be redone and which can be skipped. For more information on this feature, see `rerunning`_ 


  
Anatomical data output
^^^^^^^^^^^^^^^^^^^^^^

    After navigating to the ``$OUTPUTDIR`` you'll see the output produced by ``omni_pipeline`` is organized according to `BIDS`_ standard, where at the top level directories correspond to a unique subject (``$SUBJ``) and within that directory data is organized first, by session (``$SESSION``) followed by run (``$RUN``)

    Anatomical processing and registration with ``omni_pipeline`` involved several distinct stage, each corrsponding to a sub-directory prefixed by ``anat_proc``. For the example data set, the following directories are made:

``input_data``
..............

    Data staging: An initial staging directory that contains symbolic links (i.e., symlinks, shortcuts, aliases) to all of the raw data that will be used for preprocessing the anatomical imaging data for a particular session. Organizing links to the input files in this directory simplifies referencing them within the ``omni_pipeline`` and ``omni_preproc`` scripts.

``anat_proc_0_deoblique_anat``
..............................

    Initial data deobliquing: Many existing utilities for operating on or displaying neuroimaging data do not consistently play well with the image obliquity data located in the neuroimaging file headers. We therefore "deoblique" all of the data before doing any further processing. This does not affect the actual aquired data, only some elements in the header. In our experience, this step can spare you much confusion, frustration and heartache ``anat_proc_0_deoblique_anat`` contains "deobliqued" versions of the anatomical images which are used in all later stages of processing. "Deobliqued" anatomical files in this directory are identifiable by the suffix, "_deobliqued.nii.gz"


``anat_proc_1_debias``
......................

    Removal of low spatial frequency image intensity variation, or `debiasing`: Any image intensity bias present in the anatomical images can potentially play havoc with affine registration, estimation of synthetic images, and non-linear registration. In ``omni_pipeline`` debiasing is accomplished with the ``N4BiasFieldCorrection`` algorithm. Corrected images and intermediary files related to this process are stored here.

    ``sub-$SUBJ_ses-$SESSION_{T1w,T2w}_nm.nii.gz``: Anatomical images normalized (rescaled) so that intensity values are in the [0, 10000] range. Rescaling is suggested by the``BET`` and ``N4BiasFieldCorrection`` documentation.

    ``sub-$SUBJ_ses-$SESSION_{T1w,T2w}_debias.nii.gz``: Fully debiased input anatomical images, based on the normalized anatomical images. It's worth checking to make sure the default settings have successfully removed image bias if you see strange registration results.


``anat_proc_2_align_anat``
..........................

    Registration of multimodal anatomical images: High resolution anatomical images (T1w and T2w) must be pre-registered so that their information can be properly combined when creating the synthetic images used for field map-less distortion correction. The files present in ``anat_proc_2_align_anat`` will depend on which anatomical image (T1w or T2w) was chosen as the reference using the ``--ref`` option when calling either ``omni_pipeline`` or ``omni_synthpreproc`` (the default is ``T1``).

    ``sub-$SUBJ_ses-$SESSION_{T1w,T2w}_debias_t{1,2}space.nii.gz``: Anatomical images aligned to the selected reference image (T1w or T2w). If the T1w image is chosen as the reference the suffix will be ``_t2space.nii.gz``, whereas when the T2w image is the chosen reference the suffix will be ``_t1space.nii.gz``

    ``t{1,2}_to_t{1,2}_xfm.aff12.1D``: An file representing the affine transformation that aligns the non-reference anatomical image to the reference anatomical image. This transformation is stored in `AFNI`_ compatible format.

    ``t{1,2}_to_t{1,2}_xfm.mat``: An file representing the affine transformation that aligns the non-reference anatomical image to the reference anatomical image. This transformation is stored in `FSL`_ compatible format.

``anat_proc_3_brain_extraction``
................................

    Creating a binary anatomical brain mask: ``omni_pipeline`` or ``omni_synthpreproc`` scripts rely on ``BET`` to create binary masks that to label brain and non-brain areas of the aligned T1w and T2w anatomical images. The files present in ``anat_proc_3_brain_extraction`` will depend on the selected ``--bet_method`` option (the default is ``Norm``).

    ``norm_anat.nii.gz``: This file will be present when ``--bet_method Norm`` is selected. It corresponds to the Euclidean norm of the T1w and T2w images (after debiasing and scaling to [0 1] range of values). We've found that ``BET`` performs most reliably (particularly with lesion data) when this image is used.

    ``anat_bet_mask.nii.gz``: A binary brain only mask derived ``BET`` based on the the selected ``--bet_method``

    ``anat_eye_mask.nii.gz``: A binary mask that includes the eyes (and a little extra). This mask contributes to building the regression weight image used later when creating synthetic aligment target images.

    ``sub-$SUBJ_ses-$SESSION_{T1w,T2w}_debias_t{1,2}space_bet.nii.gz``: Brain only images, produced by multiplying the debiased T1w and T2w images by the binary brain mask constructed by ``BET``. The file suffix will vary depending on the ``--ref`` optioned used. For instance, if the T1w image is chosen as the reference the suffix will be ``_t1space_bet.nii.gz``, whereas when the T2w image is the chosen reference the suffix will be ``_t2space_bet.nii.gz``


``anat_proc_4_align_atlas``
...........................

    This directory contains file related to the registration of the subject's anatomical images to the selected group atlas. Atlas alignment transformations are estimated using brain extracted T1w anatomical images.

    ``sub-$SUBJ_ses-$SESSION_{T1w,T2w}_debias_{atlas}nii.gz``: Anatomical images aligned to the atlas space (e.g., MNI).

    ``atlas_align_affine.aff12.1D``: A file containing the `AFNI`_ formatted affine transformation that aligns the selected reference anatimomical image to the selected atlas.
    
    ``atlas_align_affine.mat``: A file containing the `FSL`_ formatted affine transformation that aligns the selected reference anatomical image to the selected atlas.
    
    ``atlas_anat_align_affine.aff12.1D``: A file containing the `AFNI`_ formatted transformation that aligns the non-reference anatomical image to the selected atlas. It is a single transformation representing the non-reference anatomical -> reference anatomical -> atlas affine transformation.


``anat_proc_5_deoblique_weight_mask_and_autobox``
.................................................

Intermediary files related to image cropping, contrast adjustment, and creation of synthetic image "weight" volumes are stored here. 

* Image cropping is important for reducing the amount of memory consumed by synthetic image creation and distortion correction. ``omni_pipeline`` and ``omni_preproc`` rely on `AFNI`_'s 3dAutobox function to perform image cropping operations. Cropped files contain``_ab`` in their file names.

* Two contrast modifying operations are performed on the the anatomical images:  
    
    #. Saturation, which compresses any extreme outlier or 'spike' voxels in the anatomical images.  Synthetic images based on images with extrema voxels tend to be unreliable. We therefore compress the images by processing them with a sigmoidal intensity function.

    #. Local histogram normalization, which distributes the intensity values of voxels in the anatomical images more evenly over the allowable range of values. Visually this has the effect of increasing the contrast of the images. This procedure also mitigates an issue that can occur when voxel intensities are compressed into a small range of values. In such a case, radial basis function decompositions of the imageas can be very inefficient. This is because in order to "bin" such a narrow range of data, many largely useless radial basis function components must be used. By normalizing the voxel intensity histograms first, radial basis function decomposition is able divide the images into meaningful and useful intensity bins using fewer components (and therefore less memory). Files that have had the local histogram normalization applied to them contain the string, `_lce` in their names. 

* An anatomical weight image is also constructed during this state. This image highlights regions of the anatomical images whose contribution is emphasized when creating synthetic images for field map-less distortion correction. Weight masks are constructed to emphasize the head, and exclude irrelevant exterior voxels. In addition brain matter voxels are given addition emphasis.



``weight_mask.nii.gz``: Image used to emphasize particularly important areas of an image when constructing synthetic images.

``weight_mask_ab.nii.gz``: Cropped weight image.

``anat_bet_mask_ab.nii.gz``: Cropped brain only mask. Here it is used to add extra emphasis to brain voxels in the weight mask.

``sub-$SUBJ_ses-$SESSION_{T1w,T2w}_debias_t{1,2}space_ab.nii.gz``: Cropped anatomical images.

``sub-$SUBJ_ses-$SESSION_{T1w,T2w}_debias_t{1,2}space_ab_sat.nii.gz``: Saturated (despiked, or compressed) cropped anatomical images.

``sub-$SUBJ_ses-$SESSION_{T1w,T2w}_debias_t{1,2}space_ab_sat_lce.nii.gz``: Histogram normalized, cropped and saturated anatomical images. These should look more "contrasty" than the original anatomical images.

``sub-$SUBJ_ses-$SESSION_{T1w,T2w}_debias_t{1,2}space_ab_sat_lce_bet.nii.gz``: Only the brains from the histogram normalized, saturated, and cropped anatomical images.

Functional data output
^^^^^^^^^^^^^^^^^^^^^^

``input_data``
..............

    Data staging: An initial staging directory that contains symbolic links (i.e., symlinks, shortcuts, aliases) to all of the raw data that will be used for preprocessing the functional imaging data for a particular session. Organizing links to the input files in this directory simplifies referencing them within the ``omni_pipeline`` and ``omni_preproc`` scripts.

``func_proc_0_deoblique_func``
..............................

    Initial data deobliquing: Many existing utilities for operating on or displaying neuroimaging data do not consistently play well with the image obliquity data located in the neuroimaging file headers. We therefore "deoblique" all of the data before doing any further processing. This does not affect the actual aquired data, only some elements in the header. In our experience, this step can spare you much confusion, frustration and heartache ``func_proc_0_deoblique_func`` contains "deobliqued" versions of the functional images which are used in all later stages of processing. "Deobliqued" functional data files in this directory are identifiable by the suffix, "_deobliqued.nii.gz"


``func_proc_1_create_reference_and_moco``
.........................................

    Contains motion corrected functional data as well as a reference functional image created from a subset of the frames least likely to be corrupted by artifacts (hopefully!). Simultaneous slice time corrections and rigidy body alignment parameters are estimated and applied to the functional data using the `SpaceTimeRealign`_ algorithm. Next, a DVARs time series is calculated for the aligned data set for the entire run. The DVARs time series is then smoothed. 100 frames associated with the lowest smoothed DVARs values are extracted. The reference functional image is created by computing the average of these frames.


    ``rigid_body.params``: Framewise motion parameters estimated by `SpaceTimeRealign`_. This file can be useful when creating movement-related nuissance regressors for de-noising functional data.

    ``sub-$SUBJ_ses-$SESSION_$TASK_$RUN_deobliqued_moco.nii.gz``: A slice time and motion corrected version of the input functional data.
    
    ``sub-$SUBJ_ses-$SESSION_$TASK_$RUN_deobliqued_reference.nii.gz``: Reference functional image.

``func_proc_2_debias``
......................

    As with anatomical images, functional data may also suffer from low spatial frequency variation in signal intensity. This form of bias can interfer with registration and distortion correction procedures. ``omni_pipeline`` and ``omni_preproc`` scripts employ the `N4BiasFieldCorrection` to estimate and remove this bias.

    ``sub-$SUBJ_ses-$SESSION_$TASK_$RUN_deobliqued_reference_nm.nii.gz``: Reference functional image rescaled so that values are in the [0, 10000] range.
    
    ``sub-$SUBJ_ses-$SESSION_$TASK_$RUN_deobliqued_reference_debias.nii.gz``: Debiased reference functional image. If you observe poor field map-less distortion correction when using ``omni_pipeline`` ``omni_preproc``, take a close look at this file and make sure that debiasing has been successful and also has not introduced any artifacts. Most `N4BiasFieldCorrection`  parameters can be adjusted using the optional command line arguments for ``omni_pipeline`` ``omni_preproc``.


``func_proc_3_brain_extraction``
................................

    This directory contains files related to constructing a brain-only binary mask and functional image. Brain-only images are useful when performing affine registration between functional and anatomical images since the large difference in skull intensity in both types of images can cause registration wackiness.

    ``sub-$SUBJ_ses-$SESSION_$TASK_$RUN_deobliqued_reference_debias_bet_mask.nii.gz``: Binary brain-only mask for the functional data.

    ``sub-$SUBJ_ses-$SESSION_$TASK_$RUN_deobliqued_reference_debias_bet.nii.gz``: Brain-only functional image created by multipying the mask by reference functional image. This image is used for affine registration later on.


``func_proc_4_autobox_and_normalize``
.....................................

    This directory contains version of the functional data that have been cropped and scaled. Cropping is performed with `AFNI`_'s 3dAutobox function. All cropped files have `_ab` in their name.

    ``sub-$SUBJ_ses-$SESSION_$TASK_$RUN_*_ab.nii.gz``: Cropped functional data data files.

``epi_proc_0_align_affine_epi_to_anat``
.......................................

    This directory contains files related to estimating the initial affine alignment parameters that register the funcitonal images to the high resolution anatomical images. Great care is taken to make this registration as accurate as possible to maximize the accuracy of the synthetic EPI image estimation procedure and to minimize the amount of non-linear registration that must be done to align the distorted functional images to the undistorted anatomical counterparts.

    This affine registration procedure is as follows:

        #. An rough inital affine alignment is estimated by aligning the brain-only reference functional image to the brain-only T2w anatomical image. This affine alignment estimate is then used as an initial estimated for ``omni_synthtarget``.


        #. ``omni_synthtarget`` is then run in a pyramidal increasing resolution scheme. In this approach a synthetic EPI images modelling the functional image are estimated at 4mm, 2mm, and finally 1mm resolution. At each resolution level the affine alignment parameters from the previous iteration are used as an initial estimate for the higher resolution. 


    ``initial_func_to_t2.aff12.1D``: File containing the initial affine alignment parameters estimated by aligning the brain-only functional reference image to the brain-only T2w anatomical image. Affine is stored in `AFNI`_ format.

    ``initial_func_to_t2.mat``: Same transformation as above, but in `FSL`_ format.

    ``initial_t2_to_func.affine``: Initial affine transformation that aligns the anatomical images to the reference functional image. This matrix is stored on ``omni`` format for use with ``omni_synthtarget``. It is simply the inverse of the ``initial_func_to_t2.aff12.1D`` or ``initial_func_to_t2.mat`` transformations.

    ``initial_func_to_t2.nii.gz``: The brain-only functional reference image aligned and upsample to the resolution of the T2w image.

    ``sub-$SUBJ_ses-$SESSION_{T1w,T2w}_debias_t{1,2}space_ab_sat_lce_{4,2,1}mm.nii.gz``: Source anatomical T1w and T2w images resampled according the resolution pyrmaid scheme. This files are used for repeated calls to ``omni_synthtarget``

    ``sub-$SUBJ_ses-$SESSION_$TASK_$RUN_deobliqued_reference_debias_bet_ab_{4,2,1}mm.nii.gz`: Target EPI/functional images used at for each level of the resolution pyramid. ``omni_synthtarget`` is used to create an undistorted synthetic version of these images using information from the corresponding ``sub-$SUBJ_ses-$SESSION_{T1w,T2w}_debias_t{1,2}space_ab_sat_lce_{4,2,1}mm.nii.gz`` files. 

    ``synthtarget_{4,2,1}mm.nii.gz``: Undistorted synthetic functional images estimated at each level of the resolution pyramid. By the time the 1mm image is complete, it should look pretty close to an undistorted version of the
    ``sub-$SUBJ_ses-$SESSION_$TASK_$RUN_deobliqued_reference_debias_bet_ab_1mm.nii.gz`` file but with more anatomical detail.

    ``final_anat_to_epi.aff12.1D``: The last affine estimated produced by ``omni_synthtarget`` running at the final pyramid resolution. Transformation stored in `AFNI`_ format. After this state, affine parameters are held fixed, and all other adjustments are non-linear distortion corrections performed during the next stage.

``epi_proc_1_distortion_correction``
....................................

    This directory contains intermediary file produced during the distorion correcting phase of preprocessing.


    ``ref_fov_mask.nii.gz``: Binary field of view mask for the synthetic EPI image defined as the intersection T1w and T2w fields of view after alignment. Because the T1w and T2w anatomical images may have a different field of view than the functional images, this binary mask is used in conjunction with ``source_fov_mask.nii.gz`` to constrain distortion correcting warps to regions of the image where the fields of view for the anatomical (and synthetic functional/EPI) and functional images overlap.

    ``source_fov_mask.nii.gz``: Binary field of view mask indicating the portion of the anatomical (and synthetic functional/EPI) field of view that corresponds to the field of view in the functional data.


``epi_proc_1_combine_transforms``
.................................



.. _rerunning:

Rerunning ``omni_pipeline`` output
----------------------------------



.. _dcm2niix: https://github.com/rordenlab/dcm2niix

.. _BIDS: https://bids.neuroimaging.io/

.. _omni gitlab: https://gitlab.com/vanandrew/omni/

.. _omni gitlab issues: https://gitlab.com/vanandrew/omni/-/issues

.. _FSL: https://fsl.fmrib.ox.ac.uk/fsl/fslwiki

.. _AFNI: https://afni.nimh.nih.gov/

.. _ANTs: http://stnava.github.io/ANTs/

.. _SpaceTimeRealign: http://nipy.org/nipy/api/generated/nipy.algorithms.registration.groupwise_registration.html#spacetimerealign
