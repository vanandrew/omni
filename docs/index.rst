omni Documentation
------------------

Welcome to the documentation page for the ``omni`` neuroimaging preprocessing library. ``omni`` is a set of utilities and scripts used for aligning and correcting multimodal neuroimaging data. Here, you will find documentation, illustrations and commentary explaining how to use different scripts and command line utilities. The ``omni`` library currently consists of two parts. The first is ``synth``, which is a set of scripts and utilities that perform field map-less correction for distorted echo planar fMRI images by non-linearly registering them to a synthetic target EPI image (`as detailed in this manuscript`_). The second part, **General Utilities**, is a set of command line utilities than may be helpful outside of the context of image registration.

The documentation is work-in-progress. While we hope to have answered the key questions users have, it is possible —probable even— that we have either overlooked some important concept in the documentation, or have not provided a sufficienly lucid explanation. We invite users to ask any clarifying questions on the `omni gitlab issues`_ page. We will do our best to answer the questions quickly and incorporate suggested improvements into the documentation.

.. note::

    If any of the ``omni`` utilities significantly assist with your research, please cite Montez, D. F., Van, A. N., Miller, R. L., Seider, N. A., Marek, S., Zheng, A., et al. (2021). Using synthetic MR images for field map-less distortion correction. bioRxiv, 2021.03.13.435270. http://doi.org/10.1101/2021.03.13.435270


Contents
++++++++

.. toctree::
    :maxdepth: 2

    Installation <installation.rst>

    Examples <omni_pipeline_walkthrough>

    Synth <synth/index>

    General Utilities <general_utilities/index>
    
    API documentation <api>
    

.. _omni gitlab issues: https://gitlab.com/vanandrew/omni/-/issues

.. _as detailed in this manuscript: https://www.biorxiv.org/content/10.1101/2021.03.13.435270v1
