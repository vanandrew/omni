# General Utilties

## Miscellaneous
```{toctree}
---
maxdepth: 2
---
miscellaneous/index
```

## Preprocessing
```{toctree}
---
maxdepth: 2
---
preprocessing/index
```

## Registration
```{toctree}
---
maxdepth: 2
---
registration/index
```

## Transforms
```{toctree}
---
maxdepth: 2
---
transforms/index
```
