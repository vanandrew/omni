**omni_extract**
================

Overview
--------

Example commands
----------------
.. code-block:: shell
	
    omni_extract

Arguments
---------

Required arguments
++++++++++++++++++

.. add_argument:: omni.scripts.extract input_image

.. add_argument:: omni.scripts.extract output_image

Optional arguments
++++++++++++++++++

.. add_argument:: omni.scripts.extract limits

.. add_argument:: omni.scripts.extract version

.. add_argument:: omni.scripts.extract help
