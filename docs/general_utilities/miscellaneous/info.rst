**omni_info**
=============

Overview
--------

Example commands
----------------
.. code-block:: shell
	
    omni_info

Arguments
---------

Required arguments
++++++++++++++++++

.. add_argument:: omni.scripts.info input_image

Optional arguments
++++++++++++++++++

.. add_argument:: omni.scripts.info version

.. add_argument:: omni.scripts.info help
