**omni_affine**
===============

Overview
--------

Example commands
----------------
.. code-block:: shell
	
    omni_affine

Arguments
---------

Required arguments
++++++++++++++++++

.. add_argument:: omni.scripts.affine output

Optional arguments
++++++++++++++++++

.. add_argument:: omni.scripts.affine input_affine

.. add_argument:: omni.scripts.affine omnify

.. add_argument:: omni.scripts.affine afnify

.. add_argument:: omni.scripts.affine fslify

.. add_argument:: omni.scripts.affine target

.. add_argument:: omni.scripts.affine source

.. add_argument:: omni.scripts.affine rigid_transform

.. add_argument:: omni.scripts.affine six_body

.. add_argument:: omni.scripts.affine invert

.. add_argument:: omni.scripts.affine version

.. add_argument:: omni.scripts.affine help
