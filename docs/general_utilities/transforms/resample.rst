**omni_resample**
=================

Overview
--------

Example commands
----------------
.. code-block:: shell
	
    omni_resample

Arguments
---------

Required arguments
++++++++++++++++++

.. add_argument:: omni.scripts.resample target_image

.. add_argument:: omni.scripts.resample input_image

.. add_argument:: omni.scripts.resample output_image

Optional arguments
++++++++++++++++++

.. add_argument:: omni.scripts.resample affine

.. add_argument:: omni.scripts.resample rigid_transform

.. add_argument:: omni.scripts.resample number_of_threads

.. add_argument:: omni.scripts.resample version

.. add_argument:: omni.scripts.resample help
