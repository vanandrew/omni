**omni_deoblique**
==================

Overview
--------

Example commands
----------------
.. code-block:: shell
	
    omni_deoblique

Arguments
---------

Required arguments
++++++++++++++++++

.. add_argument:: omni.scripts.deoblique input_image

.. add_argument:: omni.scripts.deoblique output_image

Optional arguments
++++++++++++++++++

.. add_argument:: omni.scripts.deoblique version

.. add_argument:: omni.scripts.deoblique help
